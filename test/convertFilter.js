/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const expect = require("expect");
const {FilterParsingError} = require("@eyeo/webext-ad-filtering-solution/adblockpluscore/lib/filters/index.js");

const {convertFilter} = require("../lib/converter");

// Note that most of the tests for checking the actual rules generated are
// upstream. This is just one or two smoke tests to check the integration.
describe("convertFilter", function() {
  it("should generate request blocking rules", async function() {
    let rules = await convertFilter("||example.com");
    expect(rules).toEqual([
      {
        priority: expect.any(Number),
        condition: {
          urlFilter: "||example.com",
          isUrlFilterCaseSensitive: false
        },
        action: {type: "block"}
      }
    ]);
  });

  it("should error for an unsupported filter", async function() {
    await expect(convertFilter("! this is a comment"))
      .rejects.toThrow(FilterParsingError);
  });

  it("should accept a valid regex filter", async function() {
    let rules = await convertFilter("/\\.example\\.com/.*[a-z0-9]{4}/$script");
    expect(rules).toEqual([
      {
        priority: expect.any(Number),
        condition: {
          isUrlFilterCaseSensitive: false,
          regexFilter: "\\.example\\.com\\/.*[a-z0-9]{4}",
          resourceTypes: ["script"]
        },
        action: {
          type: "block"
        }
      }
    ]);
  });

  it("should reject an invalid regex filter", async function() {
    await expect(convertFilter("/\\.example\\.com/.*[a-z0-9]{100,}/$script"))
      .rejects.toThrow(FilterParsingError);
  });

  it("should normalize filter text before conversion", async function() {
    let rules = await convertFilter("f    $    o    $    o    $    csp=f o o ");
    expect(rules).toEqual([
      {
        priority: expect.any(Number),
        condition: {
          resourceTypes: [
            "main_frame",
            "sub_frame"
          ],
          urlFilter: "f$o$o",
          isUrlFilterCaseSensitive: false
        },
        action: {
          responseHeaders: [{
            header: "Content-Security-Policy",
            operation: "append",
            value: "f o o"
          }],
          type: "modifyHeaders"
        }
      }
    ]);
  });
});

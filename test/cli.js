/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const {spawn} = require("child_process");
const path = require("path");
const expect = require("expect");

describe("command line interface", function() {
  function cliScriptPath() {
    let scriptDir = __dirname;
    let projectRoot = path.join(scriptDir, "..");
    let cliScript = path.join(projectRoot, "abp2dnr.js");
    return path.normalize(cliScript);
  }

  async function runCliScript(stdin) {
    let stdout = [];
    let stderr = [];
    let subprocess = spawn("node", [cliScriptPath()]);
    let status = await new Promise(resolve => {
      subprocess.stdin.write(stdin.join("\n"));
      subprocess.stdin.end();

      subprocess.stderr.on("data", line => stderr.push(line.toString()));
      subprocess.stdout.on("data", line => stdout.push(line.toString()));
      subprocess.on("close", code => resolve(code));
    });

    return {
      stdout,
      stderr,
      status,
      rules: JSON.parse(stdout.join("\n"))
    };
  }

  it("converts any rules in a filter list passed on the command line", async function() {
    let stdin = [
      "[Adblock Plus]",
      "",
      "/image-from-subscription.png",
      "###elem-hide",
      "/second-image.png"
    ];

    let {rules, stderr, status} = await runCliScript(stdin);

    expect(rules).toEqual([
      {
        priority: expect.any(Number),
        condition: {
          urlFilter: "/image-from-subscription.png",
          isUrlFilterCaseSensitive: false
        },
        id: 1,
        action: {type: "block"}
      },
      {
        priority: expect.any(Number),
        condition: {
          urlFilter: "/second-image.png",
          isUrlFilterCaseSensitive: false
        },
        id: 2,
        action: {type: "block"}
      }
    ]);
    expect(stderr).toEqual([]);
    expect(status).toEqual(0);
  });

  it("reports any invalid filters", async function() {
    let stdin = [
      "[Adblock Plus]",
      "",
      "/image-[a-z0-9]{100,}.png/"
    ];

    let {rules, stderr, status} = await runCliScript(stdin);

    expect(rules).toEqual([]);
    expect(stderr).toEqual([]);
    expect(status).toEqual(0);
  });
});

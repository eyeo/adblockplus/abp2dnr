# abp2dnr

[![npm](https://img.shields.io/npm/v/@eyeo/abp2dnr)](https://www.npmjs.com/package/@eyeo/abp2dnr)

This is a script to convert [Adblock Plus filter
lists](https://adblockplus.org/filters) to [chrome.declarativeNetRequest
rulesets](https://developer.chrome.com/extensions/declarativeNetRequest).

API docs are available at <https://eyeo.gitlab.io/adblockplus/abc/abp2dnr/>.

## Requirements

Before you begin, make sure to install:

- [Node.js](https://nodejs.org) version 18 or higher.
- [Node-gyp's requirements for your
system](https://github.com/nodejs/node-gyp#on-unix). This is generally Python and a C++ compiler, which is used to build RE2, which is used by this package.

After these prerequisites are met, you can add abp2dnr to your project with `npm
install`.

## Installation

`abp2dnr` is available as a NPM module:
[`@eyeo/abp2dnr`](https://www.npmjs.com/package/@eyeo/abp2dnr).

```sh
npm install --save @eyeo/abp2dnr
```

## Usage

### Command line interface

abp2dnr can be called on the command line. When you do, it will accept a filter
list on stdin and print a DNR ruleset to stdout.

```sh
cat easylist.txt | npx @eyeo/abp2dnr > ruleset.json
```

### API

abp2dnr can also be used in a script. This exposes the function `convertFilter`,
which converts a single filter text into one or more DNR rule. Note that, unlike
the CLI script, these rules do not have IDs, so assigning IDs and assembling
into a ruleset is up to you.

```js
import {convertFilter} from "@eyeo/abp2dnr";

let filters = [
  "-popup-ad."
];

let nextId = 1;
let ruleset = [];

for (let filter of filters)
{
  for (let rule of await convertFilter(filter))
  {
    rule.id = nextId++;
    ruleset.push(rule);
  }
}

console.log(JSON.stringify(ruleset));
```

You can also use it to just validate the Regular Expressions that can appear in
filter texts, to ensure that they will work when used in DNR rules.

```js
import {isRegexSupported} from "@eyeo/abp2dnr";

let validRegexSupportedResult = isRegexSupported({
  regex: "[a-z0-9]+",
  isCaseSensitive: false,
  requireCapturing: false
});

console.log(validRegexSupportedResult);
// { isSupported: true }


let invalidRegexSupportedResult = isRegexSupported({
  regex: "[a-z0-9]{1000,}",
  isCaseSensitive: false,
  requireCapturing: false
});

console.log(invalidRegexSupportedResult);
// { isSupported: false, reason: 'memoryLimitExceeded' }
```

## Contributing / Development

Documentation on how to work with abp2dnr as a developer is in
[CONTRIBUTING.md](https://gitlab.com/eyeo/adblockplus/abc/abp2dnr/-/blob/main/CONTRIBUTING.md).

## Other resources

Chromium has a [Chromium's built-in filter list
converter](https://source.chromium.org/chromium/chromium/src/+/master:extensions/browser/api/declarative_net_request/filter_list_converter/),
however it appears that this has not been kept up to date with changes to the
DNR rule syntax and defaults.
